FROM tomcat

MAINTAINER s.v.siva.kumar.ponnaganti@ericsson.com

RUN rm -rf /usr/local/tomcat/webapps/*.war

CMD ["catalina.sh", "run"]
